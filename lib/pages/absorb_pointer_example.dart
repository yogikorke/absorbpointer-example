import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AbsorbPointerExample extends StatefulWidget {
  final String title;

  AbsorbPointerExample(this.title);

  @override
  _AbsorbPointerExampleState createState() => _AbsorbPointerExampleState();
}

class _AbsorbPointerExampleState extends State<AbsorbPointerExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Fluttertoast.showToast(msg: "Hello World !");
              },
              child: Text("I will respond to Touch & Tap events"),
            ),
            SizedBox(
              height: 8,
            ),
            AbsorbPointer(
              absorbing: true,
              child: ElevatedButton(
                onPressed: () {
                  Fluttertoast.showToast(msg: "Hello World !");
                },
                child: Text(
                    "I will NOT respond to Touch & Tap events because my parent is AbsorbPointer"),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(
                    8,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
